## Server

Before launching applications, make sure that you have installed, started and configured the server.
In my project I used an Open Server and Mysql.
Database Parameters:

Database name: "testCRUD",
username: "root",
password: ""
host: "localhost",

## Installation

```bash
npm install
```

## Usage

```bash
npm run dev
```
