import axios from "axios";

export default class DepartmentService {
  static async getAllDepartments() {
    const response = await axios.get("http://localhost:5000/department");
    return response;
  }

  static async addDepartment(name, description) {
    const response = await axios.post("http://localhost:5000/department", {
      name,
      description,
    });
    return response;
  }
  static async getDepartment(id) {
    const response = await axios.get(`http://localhost:5000/department/${id}`);
    return response;
  }
  static async patchDepartment(id, name, description) {
    const response = await axios.put(`http://localhost:5000/department/${id}`, {
      name,
      description,
    });
    return response;
  }
  static async deleteDepartment(id) {
    const response = await axios.delete(
      `http://localhost:5000/department/${id}`
    );
    return response;
  }
}
