import axios from "axios";

export default class EmployeeService {
  static async getAllEmployees() {
    const response = await axios.get("http://localhost:5000/employee");
    return response;
  }
  static async getEmployee(id) {
    const response = await axios.get(`http://localhost:5000/employee/${id}`);
    return response;
  }
  static async addEmployee(firstName, lastName, department_id) {
    console.log(firstName, lastName, department_id);
    const response = await axios.post(`http://localhost:5000/employee`, {
      firstName,
      lastName,
      department_id,
    });
    return response;
  }

  static async patchEmployee(id, firstName, lastName, department_id) {
    const response = await axios.put(`http://localhost:5000/employee/${id}`, {
      firstName,
      lastName,
      department_id,
    });
    return response;
  }

  static async deleteEmployee(id) {
    const response = await axios.delete(`http://localhost:5000/employee/${id}`);
    return response;
  }
}
