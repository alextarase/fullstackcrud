import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import Department from "../pages/Department";
import Employee from "../pages/Employee.jsx";
import DepartmentInfo from "./../pages/DepartmentInfo";
import EmployeeInfo from "./../pages/EmployeeInfo";

const AppRouter = () => {
  return (
    <Routes>
      <Route path="/" element={<Department />} />
      <Route path="/department/:id" element={<DepartmentInfo />} />
      <Route path="/employee/:id" element={<EmployeeInfo />} />
      <Route path="/employee" element={<Employee />} />
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  );
};

export default AppRouter;
