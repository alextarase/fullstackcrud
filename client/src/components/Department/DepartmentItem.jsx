// @ts-nocheck
import React, { useState } from "react";
import { Button, Card, Form, InputGroup, Modal } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { editDepartment, deleteDepartment } from "../../store/departmentSlice";
import { useNavigate } from "react-router-dom";

const DepartmentItem = ({ ...props }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);
  const [name, setName] = useState(props.department.name);
  const [description, setDescription] = useState(props.department.description);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleChangeName = (e) => setName(e.target.value);
  const handleChangeDescription = (e) => setDescription(e.target.value);

  const patchDepartment = () => {
    dispatch(
      editDepartment({
        department_id: props.department.department_id,
        name: name,
        description: description,
      })
    );
  };
  const removeDepartment = () => {
    const result = window.confirm("Remove department?");
    if (result) {
      dispatch(
        deleteDepartment({ department_id: props.department.department_id })
      );
    }
  };

  const getDepartmentInfo = () => {
    navigate(`/department/${props.department.department_id}`);
  };

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit department information</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup>
            <Form.Control
              onChange={handleChangeName}
              value={name}
            ></Form.Control>
            <Form.Control
              onChange={handleChangeDescription}
              value={description}
            ></Form.Control>
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              handleClose();
              patchDepartment();
            }}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <Card
        style={{
          width: "18rem",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <Card.Body>
          <Card.Title>
            {props.department.department_id}. {props.department.name}
          </Card.Title>
          <Card.Text>{props.department.description} </Card.Text>
        </Card.Body>
        <Card.Body
          style={{
            width: "18rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            cursor: "pointer",
            gap: "20px",
          }}
        >
          <Button size="sm" variant="primary" onClick={handleShow}>
            Edit
          </Button>
          <Button size="sm" variant="danger" onClick={removeDepartment}>
            Delete
          </Button>
          <Button size="sm" variant="info" onClick={getDepartmentInfo}>
            Info
          </Button>
        </Card.Body>
      </Card>
    </>
  );
};

export default DepartmentItem;
