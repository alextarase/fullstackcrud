// @ts-nocheck
import React from "react";
import { ListGroup } from "react-bootstrap";
import { useSelector } from "react-redux";
import DepartmentItem from "./DepartmentItem";

const DepartmentList = () => {
  const { departments } = useSelector((state) => state.department);
  return (
    <ListGroup>
      {departments.map((department) => {
        return (
          <ListGroup.Item
            key={department.department_id}
            style={{
              maxWidth: "100%",
              display: "flex",
              justifyContent: "center",
              border: "none",
            }}
          >
            <DepartmentItem department={department} />
          </ListGroup.Item>
        );
      })}
    </ListGroup>
  );
};

export default DepartmentList;
