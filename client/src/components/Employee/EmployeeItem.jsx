// @ts-nocheck
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Button, Card, Form, InputGroup, Modal } from "react-bootstrap";
import { deleteEmployee, editEmployee } from "../../store/employeeSlice";

const EmployeeItem = ({ ...props }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [firstName, setFirstName] = useState(props.employee.firstName);
  const [lastName, setLastName] = useState(props.employee.lastName);
  const [department_id, setDepartment_id] = useState(
    props.employee.department_id
  );

  const handleChangeFirstName = (e) => setFirstName(e.target.value);
  const handleChangeLastName = (e) => setLastName(e.target.value);
  const handleChangeDepartment_id = (e) => setDepartment_id(e.target.value);

  const removeEmployee = () => {
    const result = window.confirm("Remove employee?");
    if (result) {
      dispatch(deleteEmployee({ employee_id: props.employee.employee_id }));
    }
  };

  const getEmployeeInfo = () => {
    navigate(`/employee/${props.employee.employee_id}`);
  };

  const patchEmployee = () => {
    dispatch(
      editEmployee({
        employee_id: props.employee.employee_id,
        firstName: firstName,
        lastName: lastName,
        department_id: department_id,
      })
    );
  };

  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit employee information</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup>
            <Form.Control
              type="string"
              placeholder="Name"
              onChange={handleChangeFirstName}
              value={firstName}
            ></Form.Control>
            <Form.Control
              type="string"
              placeholder="Surname"
              onChange={handleChangeLastName}
              value={lastName}
            ></Form.Control>
            <Form.Control
              type="number"
              min={1}
              max={50}
              placeholder="Department"
              value={department_id}
              onChange={handleChangeDepartment_id}
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              handleClose();
              patchEmployee();
            }}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <Card
        style={{
          width: "18rem",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <Card.Body>
          <Card.Title>
            {props.employee.employee_id}. {props.employee.firstName}{" "}
            {props.employee.lastName}
          </Card.Title>
        </Card.Body>
        <Card.Body
          style={{
            width: "18rem",
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            cursor: "pointer",
            gap: "20px",
          }}
        >
          <Button size="sm" variant="primary" onClick={handleShow}>
            Edit
          </Button>
          <Button size="sm" variant="danger" onClick={removeEmployee}>
            Delete
          </Button>
          <Button size="sm" variant="info" onClick={getEmployeeInfo}>
            Info
          </Button>
        </Card.Body>
      </Card>
    </>
  );
};

export default EmployeeItem;
