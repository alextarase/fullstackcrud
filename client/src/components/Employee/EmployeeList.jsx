// @ts-nocheck
import React from "react";
import { ListGroup } from "react-bootstrap";
import { useSelector } from "react-redux";
import EmployeeItem from "./EmployeeItem";

const EmployeeList = () => {
  const { employees } = useSelector((state) => state.employee);

  return (
    <ListGroup>
      {employees.map((employee) => {
        return (
          <ListGroup.Item
            key={employee.employee_id}
            style={{
              maxWidth: "100%",
              display: "flex",
              justifyContent: "center",
              border: "none",
            }}
          >
            <EmployeeItem employee={employee} />
          </ListGroup.Item>
        );
      })}
    </ListGroup>
  );
};

export default EmployeeList;
