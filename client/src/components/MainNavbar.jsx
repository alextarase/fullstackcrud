// @ts-nocheck
import React, { useState } from "react";
import { Offcanvas, Navbar, Container, Nav } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

const MainNavbar = () => {
  const navigate = useNavigate();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Navbar key={1} bg="light" expand={false} className="mb-3">
        <Container fluid>
          <Navbar.Toggle
            onClick={handleShow}
            aria-controls={`offcanvasNavbar-expand`}
          />
          <Navbar.Brand href="#">CRUD Tarasenko</Navbar.Brand>
          <Navbar.Offcanvas
            show={show}
            onHide={handleClose}
            id={`offcanvasNavbar-expand`}
            aria-labelledby={`offcanvasNavbarLabel-expand`}
            placement="start"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand`}>
                Страницы
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Nav.Link
                  onClick={() => {
                    navigate("/");
                    handleClose();
                  }}
                >
                  Departments{" "}
                </Nav.Link>
                <Nav.Link
                  onClick={() => {
                    navigate("/employee");
                    handleClose();
                  }}
                >
                  Employees
                </Nav.Link>
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    </>
  );
};

export default MainNavbar;
