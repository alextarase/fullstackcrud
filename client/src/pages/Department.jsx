// @ts-nocheck
import React, { useEffect, useState } from "react";
import DepartmentList from "../components/Department/DepartmentList";
import { useDispatch } from "react-redux";
import { fetchDepartments, createDepartment } from "../store/departmentSlice";
import { useSelector } from "react-redux";
import { Button, Form, InputGroup } from "react-bootstrap";

const Department = () => {
  const dispatch = useDispatch();
  const { status, error } = useSelector((state) => state.department);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");

  const handleChangeName = (e) => setName(e.target.value);
  const handleChangeDescription = (e) => setDescription(e.target.value);

  const createNewDepartment = () => {
    if (name && description) {
      dispatch(createDepartment({ name: name, description: description }));
      setName("");
      setDescription("");
    } else {
      alert("Fill in the fields");
    }
  };

  useEffect(() => {
    dispatch(fetchDepartments());
  }, []);

  return (
    <>
      <InputGroup>
        <Form.Control
          placeholder="Department name"
          value={name}
          onChange={handleChangeName}
        ></Form.Control>
        <Form.Control
          placeholder="Department description"
          value={description}
          onChange={handleChangeDescription}
        ></Form.Control>
        <Button onClick={createNewDepartment}>Create</Button>
      </InputGroup>
      <DepartmentList />
      {status === "loading" && <h2>Loading...</h2>}
      {error && <h2>An error: {error}</h2>}
    </>
  );
};

export default Department;
