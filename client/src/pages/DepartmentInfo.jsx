// @ts-nocheck
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchOneDepartment } from "../store/departmentSlice";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { ListGroup, Card, Button } from "react-bootstrap";

const DepartmentInfo = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { departments } = useSelector((state) => state.department);
  const department = { ...departments };

  let params = useParams();

  useEffect(() => {
    dispatch(fetchOneDepartment({ department_id: params.id }));
  }, []);

  const back = () => {
    navigate(`/`);
  };
  return (
    <ListGroup>
      <Button size="sm" variant="info" onClick={back}>
        Back
      </Button>
      <ListGroup.Item
        key={params.id}
        style={{
          maxWidth: "100%",
          display: "flex",
          justifyContent: "center",
          border: "none",
        }}
      >
        <Card
          style={{
            width: "18rem",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card.Body>
            <Card.Title>
              {department[0].department_id}. {department[0].name}
            </Card.Title>
            <Card.Text>{department[0].description} </Card.Text>
          </Card.Body>
        </Card>
      </ListGroup.Item>
    </ListGroup>
  );
};

export default DepartmentInfo;
