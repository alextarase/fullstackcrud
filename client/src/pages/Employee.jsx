// @ts-nocheck
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EmployeeList from "../components/Employee/EmployeeList";
import { fetchEmployees, createEmployee } from "../store/employeeSlice";
import { Button, Form, InputGroup } from "react-bootstrap";

const Employee = () => {
  const dispatch = useDispatch();
  const { status, error } = useSelector((state) => state.employee);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [department_id, setDepartment_id] = useState(1);

  const handleChangeFirstName = (e) => setFirstName(e.target.value);
  const handleChangeLastName = (e) => setLastName(e.target.value);
  const handleChangeDepartment_id = (e) => setDepartment_id(e.target.value);

  const createNewEmployee = () => {
    if (firstName && lastName && department_id) {
      dispatch(createEmployee({ firstName, lastName, department_id }));
      setFirstName("");
      setLastName("");
      setDepartment_id(1);
    } else {
      alert("Fill in the fields");
    }
  };

  useEffect(() => {
    dispatch(fetchEmployees());
  }, []);

  return (
    <>
      <InputGroup>
        <Form.Control
          type="string"
          placeholder="Name"
          value={firstName}
          onChange={handleChangeFirstName}
        />
        <Form.Control
          type="string"
          placeholder="Surname"
          value={lastName}
          onChange={handleChangeLastName}
        />
        <Form.Control
          type="number"
          min={1}
          max={50}
          placeholder="Department"
          value={department_id}
          onChange={handleChangeDepartment_id}
        />
        <Button onClick={createNewEmployee}>Create</Button>
      </InputGroup>
      <EmployeeList />
      {status === "loading" && <h2>Loading...</h2>}
      {error && <h2>An error: {error}</h2>}
    </>
  );
};

export default Employee;
