// @ts-nocheck
import React, { useEffect } from "react";
import { ListGroup, Card, Button } from "react-bootstrap";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchOneEmployee } from "../store/employeeSlice";

const EmployeeInfo = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { employees } = useSelector((state) => state.employee);
  const employee = { ...employees };

  let params = useParams();

  const back = () => {
    navigate(`/employee`);
  };

  useEffect(() => {
    dispatch(fetchOneEmployee({ employee_id: params.id }));
  }, []);

  return (
    <ListGroup>
      <Button size="sm" variant="info" onClick={back}>
        Back
      </Button>
      <ListGroup.Item
        key={params.id}
        style={{
          maxWidth: "100%",
          display: "flex",
          justifyContent: "center",
          border: "none",
        }}
      >
        <Card
          style={{
            width: "18rem",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Card.Body>
            <Card.Title>
              {employee[0].employee_id}. {employee[0].firstName}
            </Card.Title>
            <Card.Text>{employee[0].lastName} </Card.Text>
          </Card.Body>
        </Card>
      </ListGroup.Item>
    </ListGroup>
  );
};

export default EmployeeInfo;
