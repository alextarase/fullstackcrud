// @ts-nocheck
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import DepartmentService from "../API/DepartmentService";

export const fetchDepartments = createAsyncThunk(
  "department/fetchDepartments",
  async function (_, { rejectWithValue }) {
    try {
      const response = await DepartmentService.getAllDepartments();
      if (response.statusText !== "OK") {
        throw new Error("Fetch error!");
      }
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const fetchOneDepartment = createAsyncThunk(
  "department/fetchOneDepartment",
  async function (department, { rejectWithValue }) {
    try {
      const departmentDto = {
        department_id: department.department_id,
      };
      const response = await DepartmentService.getDepartment(
        departmentDto.department_id
      );
      if (response.statusText !== "OK") {
        throw new Error("Fetch one department error!");
      }
      return { id: response.data.department_id };
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const editDepartment = createAsyncThunk(
  "department/editDepartment",
  async function (department, { rejectWithValue, dispatch }) {
    try {
      const departmentDto = {
        department_id: department.department_id,
        name: department.name,
        description: department.description,
      };

      const response = await DepartmentService.patchDepartment(
        departmentDto.department_id,
        departmentDto.name,
        departmentDto.description
      );
      if (response.statusText !== "OK") {
        throw new Error("Edit error!");
      }
      dispatch(fetchDepartments());
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const deleteDepartment = createAsyncThunk(
  "department/deleteDepartment",
  async function (department, { rejectWithValue, dispatch }) {
    try {
      const departmentDto = {
        department_id: department.department_id,
      };

      const response = await DepartmentService.deleteDepartment(
        departmentDto.department_id
      );
      if (response.statusText !== "OK") {
        throw new Error("Delete error!");
      }
      dispatch(removeDepartment({ id: department.department_id }));
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const createDepartment = createAsyncThunk(
  "department/createDepartment",
  async function (department, { rejectWithValue, dispatch }) {
    try {
      const departmentDto = {
        name: department.name,
        description: department.description,
      };
      const response = await DepartmentService.addDepartment(
        departmentDto.name,
        departmentDto.description
      );
      if (response.statusText !== "OK") {
        throw new Error("Create error!");
      }
      const data = response.data;
      dispatch(addDepartment(data));
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const setError = (state, action) => {
  state.status = "rejected";
  state.error = action.payload;
};

const departmentSlice = createSlice({
  name: "department",
  initialState: {
    departments: [],
    status: null,
    error: null,
  },
  reducers: {
    addDepartment(state, action) {
      state.departments.push(action.payload);
    },
    removeDepartment(state, action) {
      state.departments = state.departments.filter(
        (department) => department.department_id !== action.payload.id
      );
    },
    patchDepartment(state, action) {
      const patchedDepartment = state.departments.find(
        (department) => department.id === action.payload.id
      );
      patchedDepartment.name = action.payload.name;
      patchedDepartment.description = action.payload.description;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchDepartments.pending, (state) => {
      state.status = "loading";
      state.error = null;
    }),
      builder.addCase(fetchDepartments.fulfilled, (state, action) => {
        state.status = "resolved";
        state.departments = action.payload;
      }),
      builder.addCase(fetchDepartments.rejected, () => {
        setError;
      }),
      builder.addCase(editDepartment.pending, (state) => {
        state.status = "loading";
        state.error = null;
      }),
      builder.addCase(editDepartment.fulfilled, (state) => {
        state.status = "resolved";
      }),
      builder.addCase(editDepartment.rejected, () => {
        setError;
      });

    builder.addCase(deleteDepartment.pending, (state) => {
      state.status = "loading";
      state.error = null;
    }),
      builder.addCase(deleteDepartment.fulfilled, (state) => {
        state.status = "resolved";
      }),
      builder.addCase(deleteDepartment.rejected, () => {
        setError;
      });

    builder.addCase(fetchOneDepartment.pending, (state) => {
      state.status = "loading";
      state.error = null;
    }),
      builder.addCase(fetchOneDepartment.fulfilled, (state, action) => {
        state.status = "resolved";
        state.departments = state.departments.filter(
          (department) => department.department_id === action.payload.id
        );
      }),
      builder.addCase(fetchOneDepartment.rejected, () => {
        setError;
      });
  },
});
export const { addDepartment, removeDepartment } = departmentSlice.actions;
export default departmentSlice.reducer;
