// @ts-nocheck
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import EmployeeService from "./../API/EmployeeService";

export const fetchEmployees = createAsyncThunk(
  "employee/fetchEmployees",
  async function (_, { rejectWithValue }) {
    try {
      const response = await EmployeeService.getAllEmployees();
      if (response.statusText !== "OK") {
        throw new Error("Fetch error!");
      }
      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const fetchOneEmployee = createAsyncThunk(
  "employee/fetchOneEmployee",
  async function (employee, { rejectWithValue }) {
    try {
      const employeeDto = {
        employee_id: employee.employee_id,
      };
      const response = await EmployeeService.getEmployee(
        employeeDto.employee_id
      );
      if (response.statusText !== "OK") {
        throw new Error("Fetch one employee error!");
      }
      return { id: response.data.employee_id };
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const createEmployee = createAsyncThunk(
  "employee/createEmployee",
  async function (employee, { rejectWithValue, dispatch }) {
    try {
      const employeeDto = {
        firstName: employee.firstName,
        lastName: employee.lastName,
        department_id: employee.department_id,
      };
      const response = await EmployeeService.addEmployee(
        employeeDto.firstName,
        employeeDto.lastName,
        Number(employeeDto.department_id)
      );
      if (response.statusText !== "OK") {
        throw new Error("Create error!");
      }
      const data = response.data;
      dispatch(addEmployee(data));
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const deleteEmployee = createAsyncThunk(
  "employee/deleteEmployee",
  async function (employee, { rejectWithValue, dispatch }) {
    try {
      const employeeDto = {
        employee_id: employee.employee_id,
      };
      const response = await EmployeeService.deleteEmployee(
        employeeDto.employee_id
      );
      if (response.statusText !== "OK") {
        throw new Error("Delete error!");
      }
      dispatch(removeEmployee({ id: employee.employee_id }));
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const editEmployee = createAsyncThunk(
  "employee/editEmployee",
  async function (employee, { rejectWithValue, dispatch }) {
    try {
      const employeeDto = {
        employee_id: employee.employee_id,
        firstName: employee.firstName,
        lastName: employee.lastName,
        department_id: employee.department_id,
      };
      const response = await EmployeeService.patchEmployee(
        employeeDto.employee_id,
        employeeDto.firstName,
        employeeDto.lastName,
        employeeDto.department_id
      );
      if (response.statusText !== "OK") {
        throw new Error("Edit employee error!");
      }
      dispatch(fetchEmployees());
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const employeeSlice = createSlice({
  name: "employee",
  initialState: {
    employees: [],
    status: null,
    error: null,
  },
  reducers: {
    addEmployee(state, action) {
      state.employees.push(action.payload);
    },
    removeEmployee(state, action) {
      state.employees = state.employees.filter(
        (department) => department.employee_id !== action.payload.id
      );
    },

    // patchDepartment(state, action) {},
  },
  extraReducers: (builder) => {
    builder.addCase(fetchEmployees.pending, (state) => {
      state.status = "loading";
      state.error = null;
    }),
      builder.addCase(fetchEmployees.fulfilled, (state, action) => {
        state.status = "resolved";
        state.employees = action.payload;
      }),
      builder.addCase(fetchEmployees.rejected, () => {
        setError;
      });

    builder.addCase(editEmployee.pending, (state) => {
      state.status = "loading";
      state.error = null;
    }),
      builder.addCase(editEmployee.fulfilled, (state, action) => {
        state.status = "resolved";
      }),
      builder.addCase(editEmployee.rejected, () => {
        setError;
      });

    builder.addCase(fetchOneEmployee.pending, (state) => {
      state.status = "loading";
      state.error = null;
    }),
      builder.addCase(fetchOneEmployee.fulfilled, (state, action) => {
        state.status = "resolved";
        state.employees = state.employees.filter(
          (employee) => employee.employee_id === action.payload.id
        );
      }),
      builder.addCase(fetchOneEmployee.rejected, () => {
        setError;
      });
  },
});

export const { addEmployee, removeEmployee } = employeeSlice.actions;
export default employeeSlice.reducer;
