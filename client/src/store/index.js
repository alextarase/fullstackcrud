import { configureStore } from "@reduxjs/toolkit";
import departmentReducer from "./departmentSlice";
import employeeReducer from "./employeeSlice";

export default configureStore({
  reducer: { department: departmentReducer, employee: employeeReducer },
});
