import db from "../models/index.js";

const Department = db.department;

const addDepartment = async (req, res) => {
  let info = {
    name: req.body.name,
    description: req.body.description,
  };
  const department = await Department.create(info);
  res.status(200).send(department);
  console.log("addDepartment", department);
};

const getAllDepartments = async (req, res) => {
  const departments = await Department.findAll();
  res.send(departments);
  console.log("getAllDepartments", departments);
};

const getOneDepartment = async (req, res) => {
  let id = req.params.id;
  const department = await Department.findOne({ where: { department_id: id } });
  res.send(department);
  console.log("getOneDepartment", department);
};

const updateDepartment = async (req, res) => {
  let id = req.params.id;
  const department = await Department.update(req.body, {
    where: { department_id: id },
  });
  res.status(200).send(department);
  console.log("updateDepartment", department);
};

const deleteDepartment = async (req, res) => {
  let id = req.params.id;
  const department = await Department.destroy({
    where: { department_id: id },
  });
  res.status(200).send("Deleted");
  console.log("deleteDepartment", department);
};

export default {
  addDepartment,
  getAllDepartments,
  getOneDepartment,
  updateDepartment,
  deleteDepartment,
};
