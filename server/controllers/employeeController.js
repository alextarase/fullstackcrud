import db from "../models/index.js";
const Employee = db.employee;

const addEmployee = async (req, res) => {
  let info = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    department_id: req.body.department_id,
  };
  const employee = await Employee.create(info);
  res.status(200).send(employee);
  console.log("addEmployee", employee);
};

const getAllEmployees = async (req, res) => {
  const employees = await Employee.findAll();
  res.send(employees);
  console.log("getAllEmployees", employees);
};

const getOneEmployee = async (req, res) => {
  let id = req.params.id;
  const employee = await Employee.findOne({ where: { employee_id: id } });
  res.send(employee);
  console.log("getOneEmployee", employee);
};

const updateEmployee = async (req, res) => {
  let id = req.params.id;
  const employee = await Employee.update(req.body, {
    where: { employee_id: id },
  });
  res.status(200).send(employee);
  console.log("updateEmployee", employee);
};

const deleteEmployee = async (req, res) => {
  let id = req.params.id;
  const employee = await Employee.destroy({
    where: { employee_id: id },
  });
  res.status(200).send("Deleted");
  console.log("deleteEmployee", employee);
};

export default {
  addEmployee,
  getAllEmployees,
  getOneEmployee,
  updateEmployee,
  deleteEmployee,
};
