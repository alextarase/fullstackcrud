import express from "express";
import employeeRouter from "./routes/employeeRouter.js";
import departmentRouter from "./routes/departmentRouter.js";
import cors from "cors";
const app = express();

app.use(express.json());

app.use(cors());

app.use(express.urlencoded({ extended: true }));

const routerE = employeeRouter;
const routerD = departmentRouter;
app.use("/employee", routerE);
app.use("/department", routerD);

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`server is running on port ${PORT}`);
});
