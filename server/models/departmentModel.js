import { DataTypes, Sequelize } from "sequelize";
const sequelize = new Sequelize("mysql::memory:");

export default function Department(sequelize, DataTypes) {
  const Department = sequelize.define("Department", {
    department_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  });

  return Department;
}
