import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize("mysql::memory:");
export default function Employee(sequelize, DataTypes) {
  const Employee = sequelize.define("Employee", {
    employee_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    firstName: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  });

  return Employee;
}
