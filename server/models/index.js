import Employee from "./employeeModel.js";
import Department from "./departmentModel.js";
import { Sequelize, DataTypes } from "sequelize";

const sequelize = new Sequelize("testCRUD", "root", "", {
  host: "localhost",
  dialect: "mysql",

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  define: {
    timestamps: false,
  },
});

sequelize
  .authenticate()
  .then(() => {
    console.log("connected..");
  })
  .catch((err) => {
    console.log("Error" + err);
  });

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.employee = Employee(sequelize, DataTypes);
db.department = Department(sequelize, DataTypes);

db.sequelize.sync({ force: false }).then(() => {
  console.log("re-sync done");
});

db.department.hasMany(db.employee, {
  foreignKey: "department_id",
});
db.employee.belongsTo(db.department, {
  foreignKey: "department_id",
});

export default db;
