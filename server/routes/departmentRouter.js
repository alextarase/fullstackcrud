import departmentController from "../controllers/departmentController.js";
import Router from "express";
import validateDto from "../schema/validateDto.js";
import { departmentSchema } from "../schema/department.js";
const router = Router();
router.post(
  "/",
  validateDto(departmentSchema),
  departmentController.addDepartment
);
router.get("/", departmentController.getAllDepartments);
router.get("/:id", departmentController.getOneDepartment);
router.put("/:id", departmentController.updateDepartment);
router.delete("/:id", departmentController.deleteDepartment);
export default router;
