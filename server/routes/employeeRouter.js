import employeeController from "../controllers/employeeController.js";
import Router from "express";
import validateDto from "../schema/validateDto.js";
import { employeeSchema } from "../schema/employee.js";
const router = Router();
router.post("/", validateDto(employeeSchema), employeeController.addEmployee);
router.get("/", employeeController.getAllEmployees);
router.get("/:id", employeeController.getOneEmployee);
router.put("/:id", employeeController.updateEmployee);
router.delete("/:id", employeeController.deleteEmployee);

export default router;
