import ajv from "./ajv-instance.js";

const schema = {
  type: "object",
  properties: {
    name: { type: "string" },
    description: { type: "string" },
  },
  required: ["name", "description"],
  additionalProperties: false,
};
export const departmentSchema = ajv.compile(schema);
