import ajv from "./ajv-instance.js";

const schema = {
  type: "object",
  properties: {
    firstName: { type: "string" },
    lastName: { type: "string" },
    department_id: { type: "number" },
  },
  required: ["firstName", "lastName", "department_id"],
  additionalProperties: false,
};

export const employeeSchema = ajv.compile(schema);
