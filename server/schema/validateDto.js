import ajv from "./ajv-instance.js";

function validateDto(ajv) {
  return (req, res, next) => {
    const valid = ajv(req.body);
    if (!valid) {
      const errors = ajv.errors;
      res.status(400).json(errors);
    } else {
      next();
    }
  };
}

export default validateDto;
